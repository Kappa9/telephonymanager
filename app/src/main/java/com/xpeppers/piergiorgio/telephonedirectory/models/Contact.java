/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.models;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Un contatto telefonico
 */
public class Contact extends RealmObject {
    
    /**
     * Costruttore vuoto
     */
    public Contact() {
    }
    
    @PrimaryKey
    private int id;
    private String firstName;
    private String lastName;
    private String fullName;
    private String phoneNumber;
    private String phoneNumberNoSpaces;
    
    
    //getters and setters
    
    
    public String getPhoneNumberNoSpaces() {
        return phoneNumberNoSpaces;
    }
    
    public void setPhoneNumberNoSpaces(String phoneNumberNoSpaces) {
        this.phoneNumberNoSpaces = phoneNumberNoSpaces;
    }
    
    public int getId() {
        return id;
    }
    
    public void setId(int id) {
        this.id = id;
    }
    
    public String getFirstName() {
        return firstName;
    }
    
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    
    public String getLastName() {
        return lastName;
    }
    
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    
    public String getPhoneNumber() {
        return phoneNumber;
    }
    
    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
    
    public String getFullName() {
        return fullName;
    }
    
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
}
