/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.utilities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import com.xpeppers.piergiorgio.telephonedirectory.R;


public class ManagerPermission {
    
    /**
     * Metodi interni non pubblicamente esposti
     */
    private static class Internals {
        /**
         * Verifica se un determinato permesso sia o meno stato conferito all'applicazione
         *
         * @param ctx        il contesto corrente
         * @param permission il codice del permesso in oggetto
         * @return se il permesso sia o meno stato conferito
         */
        static boolean CheckPermissions(Activity ctx, String permission) {
            int permissionState = ActivityCompat.checkSelfPermission(ctx, permission);
            return permissionState == PackageManager.PERMISSION_GRANTED;
        }
        
        /**
         * Avvia la richiesta all'utente per il conferimento di un determinato permesso
         *
         * @param ctx         il contesto corrente
         * @param permission  il codice del permesso in oggetto
         * @param requestCode il codice della richiesta. Lo stesso codice sarà restituito dal risultato della richiesta.
         */
        static void RequestPermissions(Activity ctx, String permission, int requestCode) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ctx.checkSelfPermission(permission) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(ctx,
                      new String[]{permission},
                      requestCode);
                }
            }
        }
    }
    
    /**
     * Metodo pubblico per verificare se l'utente abbia o meno conferito all'applicazione il permesso per leggere i contatti.
     *
     * @param ctx il contesto corrente
     * @return se il permesso sia o meno stato conferito
     */
    public static boolean CheckPermission_ReadContacts(Activity ctx) {
        return Internals.CheckPermissions(ctx, Manifest.permission.READ_CONTACTS);
    }
    
    
    /**
     * Metodo pubblico per avviare la richiesta per il conferimento del permesso per leggere i contatti.
     *
     * @param ctx il contesto corrente
     */
    public static void RequestPermission_ReadContacts(Activity ctx) {
        Internals.RequestPermissions(ctx, Manifest.permission.READ_CONTACTS, ctx.getResources().getInteger(R.integer.REQUESTCODE_CONTACTS_PERMISSION));
    }
}
