/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.utilities;

import java.util.regex.PatternSyntaxException;

/**
 * Classe per gestire i metodi relativi ai regex
 */
public class UtilRegex {
    
    /**
     * Calcola se una data stringa in input è valida per il regex fornito
     *
     * @param input la stringa da analizzare
     * @param regex il regex da rispettare
     * @return se la data stringa sia o meno valida per il regex fornito.
     */
    public static boolean isValidRegexString(String input, String regex) {
        try {
            return input.matches(regex);
        } catch (PatternSyntaxException e) {
            return false;
        }
    }
}
