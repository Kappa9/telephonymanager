/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.activities;

import android.app.Activity;
import android.content.ContentUris;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.telephony.PhoneNumberUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.xpeppers.piergiorgio.telephonedirectory.R;
import com.xpeppers.piergiorgio.telephonedirectory.custom_components.GenericContactInputActivity;
import com.xpeppers.piergiorgio.telephonedirectory.models.Contact;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.ManagerPermission;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilKeyboard;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilPhoneNumberFormat;

import java.util.Locale;
import java.util.Objects;

import io.realm.Realm;

import static com.xpeppers.piergiorgio.telephonedirectory.R.integer.REQUESTCODE_CONTACTS_PERMISSION;

public class ContactNewActivity extends GenericContactInputActivity {
    
    private Snackbar snackbarPermissionError = null;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contactnewactivity, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnImportFromContacts:
                checkContactPermissionOrShowPicker();
                break;
            case R.id.btnSave:
                UtilKeyboard.hideSoftKeyBoard(this);
                if (!isSaveButtonEnabled) {
                    showInvalidFieldsErrorMessage();
                    break;
                }
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        Number currentMaxContactId = realm.where(Contact.class).max("id");
                        Contact newContact = realm.createObject(Contact.class, (currentMaxContactId == null ? 0 : currentMaxContactId.intValue()) + 1);
                        newContact.setFirstName(Objects.requireNonNull(etContactName.getText()).toString().trim());
                        newContact.setLastName(Objects.requireNonNull(etContactSurname.getText()).toString().trim());
                        newContact.setPhoneNumber(Objects.requireNonNull(etContactPhoneNumber.getText()).toString().trim());
                        newContact.setPhoneNumberNoSpaces(etContactPhoneNumber.getText().toString().replace(" ", ""));
                        newContact.setFullName(String.format("%s %s", etContactSurname.getText().toString().trim(), etContactName.getText().toString().trim()));
                        realm.insert(newContact);
                    }
                });
                
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    @Override
    public void onBackPressed() {
        if (snackbarPermissionError != null && snackbarPermissionError.isShown()) {
            snackbarPermissionError.dismiss();
        } else {
            super.onBackPressed();
        }
    }
    
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        
        if (requestCode == getResources().getInteger(REQUESTCODE_CONTACTS_PERMISSION)) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //permesso garantito
                showContactPicker();
            } else {
                //per qualche motivo non hanno accettato
                
                snackbarPermissionError = Snackbar.make(rootView, R.string.permission_contacts_required,
                  Snackbar.LENGTH_INDEFINITE);
                snackbarPermissionError.setActionTextColor(Color.RED);
                snackbarPermissionError.setAction(R.string.authorize, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        checkContactPermissionOrShowPicker();
                    }
                }).show();
            }
        }
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //non posso usare lo switch in questo caso perchè non ho una costante ma una risorsa
        if (requestCode == getResources().getInteger(R.integer.REQUESTCODE_CONTACTS_PICKER)) {
            if (resultCode == Activity.RESULT_OK && data != null) {
                Uri uri = data.getData();
                if (uri != null) {
                    
                    long contactId = ContentUris.parseId(uri);
                    String selection = String.format(Locale.getDefault(), "%s = %d AND %s IN ('%s', '%s' , '%s')", ContactsContract.Data.CONTACT_ID, contactId, ContactsContract.Contacts.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Email.CONTENT_ITEM_TYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE);
                    try (Cursor cursor = getContentResolver().query(ContactsContract.Data.CONTENT_URI, null, selection, null, null)) {
                        
                        if (cursor != null) {
                            cursor.moveToFirst();
                            
                            for (String column : cursor.getColumnNames()) {
                                Log.wtf("CURSOR", String.format("%s ---> %s", column, cursor.getString(cursor.getColumnIndex(column))));
                            }
                            String contactPhoneNumber = "";
                            String contactName = "";
                            String contactSurname = "";
                            String contactFullName = "";
                            
                            while (cursor.moveToNext()) {
                                String mime = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.MIMETYPE));
                                
                                switch (mime) {
                                    case ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE:
                                        
                                        if (contactPhoneNumber.isEmpty()) {
                                            UtilPhoneNumberFormat utilPhoneNumberFormat = new UtilPhoneNumberFormat(ContactNewActivity.this);
                                            String importedPhoneNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA1));
                                            if (importedPhoneNumber != null) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                                    // lo codifico con la libreria standard perchè quando recupero il numero dal cursore
                                                    // rimuove gli spazi.
                                                    contactPhoneNumber = utilPhoneNumberFormat.normalizePhoneNumber(PhoneNumberUtils.formatNumber(importedPhoneNumber, Locale.getDefault().getCountry()));
                                                } else {
                                                    contactPhoneNumber = utilPhoneNumberFormat.normalizePhoneNumber(importedPhoneNumber);
                                                }
                                            }
                                        }
                                        
                                        if (contactFullName.isEmpty())
                                            contactFullName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                                        break;
                                    case ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE:
                                        if (contactName.isEmpty()) {
                                            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA2));
                                            if (contactName == null)
                                                contactName = "";
                                        }
                                        if (contactSurname.isEmpty()) {
                                            contactSurname = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DATA3));
                                            if (contactSurname == null)
                                                contactSurname = "";
                                        }
                                        
                                        if (contactFullName.isEmpty())
                                            contactFullName = cursor.getString(cursor.getColumnIndex(ContactsContract.Data.DISPLAY_NAME));
                                        break;
                                }
                            }
                            cursor.close();
                            
                            etContactName.setText(contactName.isEmpty() ? contactFullName : contactName);
                            etContactSurname.setText(contactSurname);
                            etContactPhoneNumber.setText(contactPhoneNumber);
                        }
                    }
                }
            }
        }
    }
    
    /**
     * Mostra il picker per scegliere quale contatto importare
     */
    private void showContactPicker() {
        Intent intent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(intent, getResources().getInteger(R.integer.REQUESTCODE_CONTACTS_PICKER));
    }
    
    /**
     * Controlla il permesso di leggere i contatti ed eventualmente lo richiede.
     */
    private void checkContactPermissionOrShowPicker() {
        if (!ManagerPermission.CheckPermission_ReadContacts(ContactNewActivity.this)) {
            ManagerPermission.RequestPermission_ReadContacts(ContactNewActivity.this);
        } else {
            showContactPicker();
        }
    }
}
