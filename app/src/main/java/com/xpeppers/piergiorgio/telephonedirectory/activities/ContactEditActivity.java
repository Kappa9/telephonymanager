/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.xpeppers.piergiorgio.telephonedirectory.R;
import com.xpeppers.piergiorgio.telephonedirectory.custom_components.CustomApplicationContext;
import com.xpeppers.piergiorgio.telephonedirectory.custom_components.GenericContactInputActivity;
import com.xpeppers.piergiorgio.telephonedirectory.models.Contact;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilKeyboard;

import java.util.Objects;

import io.realm.Realm;

public class ContactEditActivity extends GenericContactInputActivity {
    
    //manteniamo il riferimento a realm.
    private Contact contact;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        if (!checkIntentAndInitializeVariables()) {
            finish();
            return;
        }
        
        etContactName.setText(contact.getFirstName());
        etContactSurname.setText(contact.getLastName());
        etContactPhoneNumber.setText(contact.getPhoneNumber());
    }
    
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_contacteditactivity, menu);
        return true;
    }
    
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnSave:
                UtilKeyboard.hideSoftKeyBoard(this);
                
                if (!isSaveButtonEnabled) {
                    showInvalidFieldsErrorMessage();
                    break;
                }
                realm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(@NonNull Realm realm) {
                        contact.setFirstName(Objects.requireNonNull(etContactName.getText()).toString());
                        contact.setLastName(Objects.requireNonNull(etContactSurname.getText()).toString());
                        contact.setPhoneNumber(Objects.requireNonNull(etContactPhoneNumber.getText()).toString());
                        contact.setPhoneNumberNoSpaces(etContactPhoneNumber.getText().toString().replace(" ", ""));
                        contact.setFullName(String.format("%s %s", etContactSurname.getText().toString(), etContactName.getText().toString()));
                    }
                });
                
                finish();
                break;
            case R.id.btnDelete:
                new AlertDialog.Builder(this, R.style.alertDialogTheme)
                  .setTitle(getString(R.string.confirm_needed))
                  .setMessage(R.string.contact_delete_confirm)
                  .setPositiveButton(getString(R.string.delete), new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          realm.executeTransaction(new Realm.Transaction() {
                              @Override
                              public void execute(@NonNull Realm realm) {
                                  contact.deleteFromRealm();
                              }
                          });
                          finish();
                          dialog.dismiss();
                      }
                  })
                  .setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                      @Override
                      public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                      }
                  })
                  .show();
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    
    
    /**
     * Verifichiamo se i dati sono arrivati correttamente dall'activity precedente.
     *
     * @return Ritorna se i dati sono validi ed il contatto è stato trovato
     */
    private boolean checkIntentAndInitializeVariables() {
        Intent data = getIntent();
        if (data == null || !data.hasExtra(getResources().getString(R.string.constant_inputextra_contact_id))) {
            ((CustomApplicationContext) getApplicationContext()).getIShowMessageManager().showMessage(ContactEditActivity.this, getString(R.string.contact_edit_error));
            return false;
        }
        realm = Realm.getDefaultInstance();
        
        contact = realm
          .where(Contact.class)
          .equalTo("id", data.getIntExtra(getResources().getString(R.string.constant_inputextra_contact_id), -1))
          .findFirst();
        
        if (contact == null) {
            ((CustomApplicationContext) getApplicationContext())
              .getIShowMessageManager()
              .showMessage(ContactEditActivity.this, getString(R.string.contact_edit_error));
            return false;
        }
        return true;
    }
    
    @Override
    public void onBackPressed() {
        new AlertDialog.Builder(this, R.style.alertDialogTheme)
          .setTitle(R.string.confirm_needed)
          .setMessage(getResources().getString(R.string.current_edit_cancel))
          .setPositiveButton(getResources().getString(R.string.cancel_editing), new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
                  finish();
              }
          })
          .setNegativeButton(getResources().getString(R.string.keep_editing), new DialogInterface.OnClickListener() {
              @Override
              public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
              }
          })
          .show();
    }
}
