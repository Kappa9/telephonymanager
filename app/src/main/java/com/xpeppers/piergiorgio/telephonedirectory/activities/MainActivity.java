/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.activities;

import android.content.Intent;
import android.os.Handler;
import android.os.PersistableBundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.TextView;

import com.xpeppers.piergiorgio.telephonedirectory.R;
import com.xpeppers.piergiorgio.telephonedirectory.adapters.ContactAdapter;
import com.xpeppers.piergiorgio.telephonedirectory.custom_components.SimpleDividerItemDecoration;
import com.xpeppers.piergiorgio.telephonedirectory.models.Contact;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilKeyboard;

import java.util.Objects;

import io.realm.Realm;
import io.realm.RealmChangeListener;
import io.realm.RealmResults;

/**
 * Activity per la gestione della rubrica salvata.
 */
public class MainActivity extends AppCompatActivity {
    
    //region variabili locali
    private Realm realm;
    
    private RecyclerView rvPhoneNumbers;
    private TextView tvEmptyList;
    private TextInputLayout tilSearchView;
    private TextInputEditText etSearchView;
    
    private ContactAdapter contactAdapter;
    private RealmResults<Contact> contacts;
    
    private RealmChangeListener<RealmResults<Contact>> contactChangeListener = new RealmChangeListener<RealmResults<Contact>>() {
        @Override
        public void onChange(@NonNull RealmResults<Contact> contacts) {
            contactAdapter.updateData(contacts);
//            rvPhoneNumbers.setVisibility((contacts.size() > 0 && !Objects.requireNonNull(etSearchView.getText()).toString().isEmpty()) ? View.VISIBLE : View.GONE);
            tilSearchView.setVisibility(contacts.size() > 0 ? View.VISIBLE : View.GONE);
            tvEmptyList.setVisibility(contacts.size() > 0 ? View.GONE : View.VISIBLE);
        }
    };
    
    private TextWatcher textWatcherSearchView = new TextWatcher() {
        final Handler handler = new Handler();
        Runnable runnable;
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
        }
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handler.removeCallbacks(runnable);
        }
        
        @Override
        public void afterTextChanged(final Editable s) {
            runnable = new Runnable() {
                @Override
                public void run() {
//                    rvPhoneNumbers.setVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                    contactAdapter.getFilter().filter(s.toString());
                }
            };
            handler.postDelayed(runnable, 500);
        }
    };
    // endregion
    
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        etSearchView.setText(savedInstanceState.getString(getString(R.string.constant_instancestate_filter_text), ""));
        etSearchView.setSelection(Objects.requireNonNull(etSearchView.getText()).toString().length());
        
        UtilKeyboard.hideSoftKeyBoard(this);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(getString(R.string.constant_instancestate_filter_text), Objects.requireNonNull(etSearchView.getText()).toString());
    }
    
    /**
     * Nell'onDestroy andiamo a chiudere l'istanza di realm.
     */
    @Override
    protected void onDestroy() {
        super.onDestroy();
        contacts.removeAllChangeListeners();
        etSearchView.removeTextChangedListener(textWatcherSearchView);
        if (realm != null) {
            realm.close();
            realm = null;
        }
    }
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        rvPhoneNumbers = findViewById(R.id.rvPhoneNumbers);
        tvEmptyList = findViewById(R.id.tvEmptyList);
        etSearchView = findViewById(R.id.etSearchView);
        tilSearchView = findViewById(R.id.tilSearchView);
        
        realm = Realm.getDefaultInstance();
        realm.setAutoRefresh(true);
        
        loadList();
        initializeListeners();
        initializeSearch();
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        realm.refresh();
    }
    
    //region metodi interni
    
    /**
     * Inizializza la vista con filtro
     */
    private void initializeSearch() {
        etSearchView.addTextChangedListener(textWatcherSearchView);
    }
    
    /**
     * Carica la lista dei contatti
     */
    private void loadList() {
        contacts = realm.where(Contact.class).sort("fullName").findAllAsync();
        contacts.addChangeListener(contactChangeListener);
        rvPhoneNumbers.setLayoutManager(new LinearLayoutManager(this));
        rvPhoneNumbers.addItemDecoration(new SimpleDividerItemDecoration(MainActivity.this));
        contactAdapter = new ContactAdapter(MainActivity.this, contacts);
        rvPhoneNumbers.setAdapter(contactAdapter);
    }
    
    /**
     * Inizializza gli eventi
     */
    private void initializeListeners() {
        
        findViewById(R.id.fabAddContact).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ContactNewActivity.class));
            }
        });
    }
    
    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
