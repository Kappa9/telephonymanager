/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.custom_components;

import android.os.Bundle;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputEditText;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;

import com.xpeppers.piergiorgio.telephonedirectory.R;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilKeyboard;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilPhoneLocalePrefix;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.UtilRegex;

import java.util.Objects;

import io.realm.Realm;

/**
 * Visto che le due activity di crea e modifica fanno quasi la stessa cosa
 * e usano lo stesso layout, usiamo un activity generica
 */
public abstract class GenericContactInputActivity extends AppCompatActivity {
    
    protected Realm realm;
    
    protected ConstraintLayout rootView;
    protected TextInputEditText etContactName, etContactSurname, etContactPhoneNumber;
    
    protected boolean isContactNameValid, isContactSurnameValid, isContactPhoneNumberValid;
    protected boolean isSaveButtonEnabled = false;
    
    //region textWatchers
    
    private TextWatcher contactNameTextWatcher = new TextWatcher() {
        final Handler handler = new Handler();
        Runnable runnable;
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
        }
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handler.removeCallbacks(runnable);
            etContactName.setError(null);
        }
        
        @Override
        public void afterTextChanged(final Editable s) {
            
            runnable = new Runnable() {
                @Override
                public void run() {
                    isContactNameValid = s.toString().length() > 0;
                    isSaveButtonEnabled = isContactNameValid && isContactSurnameValid && isContactPhoneNumberValid;
                }
            };
            handler.postDelayed(runnable, 500);
        }
    };
    
    private TextWatcher contactSurnameTextWatcher = new TextWatcher() {
        final Handler handler = new Handler();
        Runnable runnable;
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
        }
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handler.removeCallbacks(runnable);
            etContactSurname.setError(null);
        }
        
        @Override
        public void afterTextChanged(final Editable s) {
            runnable = new Runnable() {
                @Override
                public void run() {
                    isContactSurnameValid = s.toString().length() > 0;
                    isSaveButtonEnabled = isContactNameValid && isContactSurnameValid && isContactPhoneNumberValid;
                }
            };
            handler.postDelayed(runnable, 500);
        }
    };
    
    private TextWatcher contactPhoneNumberTextWatcher = new TextWatcher() {
        final Handler handler = new Handler();
        Runnable runnable;
        
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        
        }
        
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            handler.removeCallbacks(runnable);
            etContactPhoneNumber.setError(null);
        }
        
        @Override
        public void afterTextChanged(final Editable s) {
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        checkPhoneNumberValid();
                    }
                };
                handler.postDelayed(runnable, 500);
            }
        }
    };
    
    protected void checkPhoneNumberValid() {
        isContactPhoneNumberValid = Objects.requireNonNull(etContactPhoneNumber.getText()).toString().length() > 0 && UtilRegex.isValidRegexString(Objects.requireNonNull(etContactPhoneNumber.getText()).toString(), getResources().getString(R.string.regex_phonenumber));
        isSaveButtonEnabled = isContactNameValid && isContactSurnameValid && isContactPhoneNumberValid;
    }
    
    //endregion
    
    
    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        
        isSaveButtonEnabled = savedInstanceState.getBoolean(getString(R.string.constant_instancestate_savebutton), false);
        
        etContactName.setText(savedInstanceState.getString(getString(R.string.constant_instancestate_contact_name), ""));
        etContactName.setSelection(Objects.requireNonNull(etContactName.getText()).toString().length());
        etContactSurname.setText(savedInstanceState.getString(getString(R.string.constant_instancestate_contact_surname), ""));
        etContactSurname.setSelection(Objects.requireNonNull(etContactName.getText()).toString().length());
        etContactPhoneNumber.setText(savedInstanceState.getString(getString(R.string.constant_instancestate_contact_phonenumber), ""));
        etContactPhoneNumber.setSelection(Objects.requireNonNull(etContactName.getText()).toString().length());
        
        UtilKeyboard.hideSoftKeyBoard(this);
    }
    
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(getString(R.string.constant_instancestate_contact_name), Objects.requireNonNull(etContactName.getText()).toString());
        outState.putString(getString(R.string.constant_instancestate_contact_surname), Objects.requireNonNull(etContactSurname.getText()).toString());
        outState.putString(getString(R.string.constant_instancestate_contact_phonenumber), Objects.requireNonNull(etContactPhoneNumber.getText()).toString());
        
        outState.putBoolean(getString(R.string.constant_instancestate_savebutton), isSaveButtonEnabled);
    }
    
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (realm != null) {
            realm.close();
            realm = null;
        }
        etContactName.removeTextChangedListener(contactNameTextWatcher);
        etContactSurname.removeTextChangedListener(contactSurnameTextWatcher);
        etContactPhoneNumber.removeTextChangedListener(contactPhoneNumberTextWatcher);
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_contact);
        
        rootView = findViewById(R.id.rootView);
        etContactName = findViewById(R.id.etContactName);
        etContactSurname = findViewById(R.id.etContactSurname);
        etContactPhoneNumber = findViewById(R.id.etContactPhoneNumber);
        
        realm = Realm.getDefaultInstance();
        
        //metto un listener per sbiancare gli errori
        etContactName.addTextChangedListener(contactNameTextWatcher);
        etContactSurname.addTextChangedListener(contactSurnameTextWatcher);
        etContactPhoneNumber.addTextChangedListener(contactPhoneNumberTextWatcher);
    }
    
    @Override
    protected void onResume() {
        super.onResume();
        if (Objects.requireNonNull(etContactPhoneNumber.getText()).toString().isEmpty()) {
            etContactPhoneNumber.append(new UtilPhoneLocalePrefix().prefixFor(getResources().getConfiguration().locale.getCountry()));
            etContactPhoneNumber.append(" ");
        }
    }
    
    /**
     * Mostra messaggio di errore nel caso in cui i campi siano invalidi al salvataggio.
     */
    protected void showInvalidFieldsErrorMessage() {
        if (!isContactNameValid)
            etContactName.setError(getString(R.string.error_name_missing));
        
        if (!isContactSurnameValid)
            etContactSurname.setError(getString(R.string.error_surname_missing));
        
        if (!isContactPhoneNumberValid) {
            if (Objects.requireNonNull(etContactPhoneNumber.getText()).toString().isEmpty()) {
                etContactPhoneNumber.setError(getString(R.string.error_phonenumber_missing));
            } else {
                etContactPhoneNumber.setError(getString(R.string.error_phonenumber_invalid));
            }
        }
    }
}
