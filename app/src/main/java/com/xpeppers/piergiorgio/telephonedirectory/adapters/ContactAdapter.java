/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageButton;
import android.widget.TextView;

import com.xpeppers.piergiorgio.telephonedirectory.R;
import com.xpeppers.piergiorgio.telephonedirectory.activities.ContactEditActivity;
import com.xpeppers.piergiorgio.telephonedirectory.models.Contact;

import java.lang.ref.WeakReference;

import io.realm.Case;
import io.realm.OrderedRealmCollection;
import io.realm.RealmRecyclerViewAdapter;

/**
 * Adapter per riempire una lista di contatti, presi in diretta da realm.
 */
public class ContactAdapter extends RealmRecyclerViewAdapter<Contact, ContactAdapter.MyViewHolder> implements Filterable {
    
    private OrderedRealmCollection<Contact> originalData;
    private WeakReference<Activity> ctx;
    
    
    public ContactAdapter(Activity ctx, OrderedRealmCollection<Contact> data) {
        super(data, true);
        this.originalData = data;
        this.ctx = new WeakReference<>(ctx);
        setHasStableIds(true);
    }
    
    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact, parent, false);
        return new MyViewHolder(itemView);
    }
    
    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final Contact contact = getItem(position);
        
        if (contact != null) {
            holder.contactPhoneNumber.setVisibility(View.VISIBLE);
            holder.contactFullName.setVisibility(View.VISIBLE);
            holder.ibEditContact.setVisibility(View.VISIBLE);
            
            holder.contact = contact;
            holder.contactFullName.setText(contact.getFullName());
            holder.contactPhoneNumber.setText(contact.getPhoneNumber());
            holder.ibEditContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (ctx.get() != null) {
                        Intent editIntent = new Intent(ctx.get(), ContactEditActivity.class);
                        editIntent.putExtra(ctx.get().getResources().getString(R.string.constant_inputextra_contact_id), contact.getId());
                        ctx.get().startActivity(editIntent);
                    }
                }
            });
        } else {
            holder.contactPhoneNumber.setVisibility(View.GONE);
            holder.contactFullName.setVisibility(View.GONE);
            holder.ibEditContact.setVisibility(View.GONE);
        }
    }
    
    @Override
    public long getItemId(int index) {
        //noinspection ConstantConditions
        return getItem(index).getId();
    }
    
    @Override
    public Filter getFilter() {
        return new ContactFilter(this);
    }
    
    private void filterResults(String text) {
        if (originalData == null || originalData.isEmpty()) {
            return;
        }
        text = text == null ? null : text.toLowerCase().trim();
        if (text == null || text.isEmpty()) {
            updateData(originalData);
        } else {
            updateData(originalData.where()
              .contains("fullName", text, Case.INSENSITIVE)
              .or()
              .contains("phoneNumberNoSpaces", text, Case.INSENSITIVE)
              .sort("fullName")
              .findAll());
        }
    }
    
    private class ContactFilter extends Filter {
        private final ContactAdapter adapter;
        
        private ContactFilter(ContactAdapter adapter) {
            super();
            this.adapter = adapter;
        }
        
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            return new FilterResults();
        }
        
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            adapter.filterResults(constraint.toString());
        }
    }
    
    class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView contactFullName;
        private TextView contactPhoneNumber;
        private ImageButton ibEditContact;
        
        Contact contact;
        
        MyViewHolder(View view) {
            super(view);
            contactFullName = view.findViewById(R.id.tvContactName);
            contactPhoneNumber = view.findViewById(R.id.tvContactNumber);
            ibEditContact = view.findViewById(R.id.ibEditContact);
        }
    }
}