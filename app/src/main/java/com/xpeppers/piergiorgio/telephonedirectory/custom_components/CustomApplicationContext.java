/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.custom_components;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.xpeppers.piergiorgio.telephonedirectory.interfaces.IShowMessageManager;
import com.xpeppers.piergiorgio.telephonedirectory.utilities.ManagerToast;

import io.fabric.sdk.android.Fabric;
import io.realm.Realm;
import io.realm.RealmConfiguration;

public class CustomApplicationContext extends Application {
    
    private IShowMessageManager _iShowMessageManager;
    
    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        
        //inizializzo il database
        Realm.init(getApplicationContext());
        
        //gestisco le migrazioni
        RealmConfiguration config = new RealmConfiguration.Builder()
          .schemaVersion(2)
          .migration(new CustomRealmMigration())
          .build();
        
        //inizializzo il database
        Realm.setDefaultConfiguration(config);
        
        _iShowMessageManager = new ManagerToast();
    }
    
    public IShowMessageManager getIShowMessageManager() {
        return _iShowMessageManager;
    }
}
