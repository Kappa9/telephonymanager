/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.interfaces;

import android.content.Context;

/**
 * Interfaccia per gestire i diversi metodi di visualizzazione dei messaggi (Toast, Snackbar, ...)
 * Si inizializza nell' ApplicationContext
 */
public interface IShowMessageManager {
    /**
     * Mostra il messaggio
     *
     * @param ctx il contesto attuale
     * @param msg il messaggio da mostrare
     */
    void showMessage(Context ctx, String msg);
}
