/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.utilities;

import android.content.Context;
import android.widget.Toast;

import com.xpeppers.piergiorgio.telephonedirectory.interfaces.IShowMessageManager;

/**
 * Classe per gestire i toast così che non vadano a sovrascriversi
 */
public class ManagerToast implements IShowMessageManager {
    
    public ManagerToast() {
    }
    
    private static Toast m_currentToast;
    
    @Override
    public void showMessage(Context ctx, String text) {
        try {
            if (m_currentToast != null) {
                m_currentToast.cancel();
            }
            m_currentToast = Toast.makeText(ctx, text, Toast.LENGTH_LONG);
            m_currentToast.show();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    
    public void dismissMessage() {
        if (m_currentToast != null) {
            m_currentToast.cancel();
        }
    }
}
