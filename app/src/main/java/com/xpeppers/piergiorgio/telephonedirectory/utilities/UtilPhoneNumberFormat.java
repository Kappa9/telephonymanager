/*
 * Copyright (C) 2018  Pier Giorgio Misley
 *
 *     This program is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     This program is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

package com.xpeppers.piergiorgio.telephonedirectory.utilities;

import android.content.Context;

import java.lang.ref.WeakReference;

/**
 * Classe per gestire il numero di telefono ed i relativi formati.
 * Prova a bonificare il numero di telefono, qualora necessario.
 */
public class UtilPhoneNumberFormat {
    private WeakReference<Context> ctx;
    
    public UtilPhoneNumberFormat(Context ctx) {
        this.ctx = new WeakReference<>(ctx);
    }
    
    /**
     * Spesso i numeri sono recuperati dalla rubrica con più spazi del desiderato o senza spazi.
     * Essendo l'importazione da rubrica solitamente corretta, provo a normalizzare.
     *
     * @param inputNumber il numero telefonico importato grezzo
     * @return il numero telefonico bonificato
     */
    public String normalizePhoneNumber(String inputNumber) {
//        //se inputnumber non inizia con + non ha la locale, quindi la metto io.
        if (!inputNumber.startsWith("+"))
            return normalizePhoneNumber(String.format("%s %s", new UtilPhoneLocalePrefix().prefixFor(ctx.get().getResources().getConfiguration().locale.getCountry()), inputNumber));
        
        String[] parts = inputNumber.split(" ");
        switch (parts.length) {
            case 1:
                //se la lunghezza della stringa è > 6, posso provare a dividerla
                return normalizePhoneNumber(addSpaceIfStringLengthIsMoreThan6(inputNumber));
            case 2:
                // se la seconda stringa è maggiore di 6, posso provare a dividerla.
                if (parts[1].length() > 6) {
                    return normalizePhoneNumber(String.format("%s %s", parts[0], addSpaceIfStringLengthIsMoreThan6(parts[1])));
                } else
                    return inputNumber;
            case 3:
                //se sono 3, è potenzialmente corretto.
                return inputNumber;
            default:
                //se sono più di 3, concateno gli ultimi.
                String output = String.format("%s %s %s", parts[0], parts[1], parts[2]);
                for (int i = 3; i < parts.length; i++) {
                    output = output.concat(parts[i]);
                    
                }
                return output;
        }
    }
    
    /**
     * Metodo per aggiungere lo spazio in una string che sia di dimensione superiore a 6.
     * Serve per dividere i numeri nel caso in cui il formato del numero telefonico sia errato.
     *
     * @param inputNumber la parte finale del numero di telefono
     * @return la stessa string in input, con uno spazio al punto ipoteticamente giusto.
     */
    private String addSpaceIfStringLengthIsMoreThan6(String inputNumber) {
        if (inputNumber.length() <= 6) {
            return inputNumber;
        }
        String initialPart = inputNumber.substring(0, inputNumber.length() - 6);
        String finalPart = inputNumber.substring(inputNumber.length() - 6);
        return String.format("%s %s", initialPart, finalPart);
    }
}
