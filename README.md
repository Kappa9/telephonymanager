Come compilare il programma:

1 Aprire con Android studio la cartella root del processo.
2 Verrà richiesto di indicare il percorso dell'SDK nel computer locale; di default verrà richiesto l'ultimo utilizzato.
    2.1 Nel caso in cui non dovesse essere richiesto, e nel caso in cui dovesse dar errore, è possibile andare nelle impostazioni tramite File > Settings su Android studio.
    2.2 Ricercare "SDK" e compare la voce "Android SDK Location" che occorre modificare con il percorso del computer locale.
3 A questo punto sarà sufficiente avviare la compilazione del progetto tramite "Build > Rebuild Project".
    3.1 Potrebbe essere necessario installare le ultime versioni delle librerie, ma si occuperà di tutto Android Studio lasciando come unica operazione necessaria la conferma della richiesta.

